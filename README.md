## Національний технічний університет України<br>“Київський політехнічний інститут ім. Ігоря Сікорського”

## Факультет прикладної математики<br>Кафедра системного програмування і спеціалізованих комп’ютерних систем


# Лабораторна робота №1<br>"Базова робота з git"

## КВ-11 Чебан Максим

## Хід виконання роботи

### 1. Зклонувати будь-який невеликий проєкт open-source з github

# 1.1 Клонування повного репозиторію
Для клонування будь-якого невеликого open-source проєкту, знаходимо посилання. Наприклад, нехай це буде https://github.com/hydrogen-music/hydrogen .

```
$ git clone https://github.com/hydrogen-music/hydrogen
Cloning into 'hydrogen'...
remote: Enumerating objects: 66236, done.
remote: Counting objects: 100% (5902/5902), done.
remote: Compressing objects: 100% (778/778), done.
remote: Total 66236 (delta 5358), reused 5257 (delta 5123), pack-reused 60334
Receiving objects: 100% (66236/66236), 43.31 MiB | 10.69 MiB/s, done.
Resolving deltas: 100% (48020/48020), done.

```
# 1.2 Клонування часткового репозиторію
Для клонування часткового репозиторію обераємо гілку, в данному випадку це буде releases/1.2 .
```
$ git clone https://github.com/hydrogen-music/hydrogen/ --depth=1 --single-branch --branch=releases/1.2 folder
Cloning into 'folder'...
remote: Enumerating objects: 863, done.
remote: Counting objects: 100% (863/863), done.
remote: Compressing objects: 100% (793/793), done.
remote: Total 863 (delta 227), reused 359 (delta 57), pack-reused 0
Receiving objects: 100% (863/863), 12.76 MiB | 10.08 MiB/s, done.
Resolving deltas: 100% (227/227), done.

```
Зазначимо, що 
* --depth=1 -- задає кількість комітів історії, що нам потрібна для скачування
* --single-branch -- задає гіту отримувати лише одну гілку
* --branch=releases/1.2 -- задає гіту ім'я гілки, робоче дерево якої буде створене після
клонування. В поєднанні із --single-branch задає ім'я єдиної гілки, яку треба отримати
* folder -- останній опційний параметр вказує бажане ім'я локальної папки, замість
оригінального імені репозиторію, в яку буде зклоновано проєкт.

# 1.3 Різниця в розмірі баз даних двох клонів
Порівнюємо отриманні репозеторії:

```
$ du -sh ./*/.git
14M	./folder/.git
46M	./hydrogen/.git
```
Видно, що розміри баз даних гіта є різними, оскільки скачане дерево історії у folder містить лише об'єкти, необхідні 
для відтворення одного останнього стану однієї гілки i18n
### 2. Зробити не менше трьох локальних комітів

Створюємо три файли `index.html`, `styles.css`, `program.cs` модифіковано файл `treestate.py`
Тепер продемонструємо різні способи додавання файлів до індексу 
та вказання повідомлення коміту.
* 1)

```
$ git add index.html 
$ git commit -m "file index.html was added"
[releases/1.2 aa1099d] file index.html was added
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 index.html

```
* 2)
```
$ git commit -am "file treestate.py was changed"[releases/1.2 b9f39c0] file treestate.py was changed
 1 file changed, 2 insertions(+)

```
* 3)
```
$ git add .
$ git commit -m "all files were added"
[releases/1.2 63a6920] all files were added
 2 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 program.cs
 create mode 100644 styles.css

```
### 3.Внесення змін за допомогою опції --amend.

Ця опція працює наступним чином: якщо ми внесемо певні змінни, що будуть додаватись до попереднього коміту
і використовуємо команду `git commit --amend`, то замість створення нового коміту всі зміни додадуться в попередній коміт.
```
$ git status
On branch releases/1.2
Your branch is ahead of 'origin/releases/1.2' by 3 commits.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	text.txt

nothing added to commit but untracked files present (use "git add" to track)


$ git add text.txt 
$ git commit --amend -m "text.txt was added through git amend"
[releases/1.2 6f92bff] text.txt was added through git amend
 Date: Tue Dec 12 22:08:51 2023 +0200
 3 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 program.cs
 create mode 100644 styles.css
 create mode 100644 text.txt


$ git status
On branch releases/1.2
Your branch is ahead of 'origin/releases/1.2' by 3 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean

```
Як можна помітити, кількість нових комітів не змінилась, але змінився зміст та опис коміту, який був зробленим останнім.

### 4.Об'єднання кількох останніх комітів в один за допомогою git reset.
Для об'єднання декількох останніх комітів в один, можна попросити гіт відкотитись на N комтів назад, 
але при цьому зміни залишивши в дереві.
```
$ git reset HEAD~2
Unstaged changes after reset:
M	treestate.py
student@virt-linux:~/Lab1/folder$ git status
On branch releases/1.2
Your branch is ahead of 'origin/releases/1.2' by 1 commit.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   treestate.py

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	program.cs
	styles.css
	text.txt

no changes added to commit (use "git add" and/or "git commit -a")

```
Гіт скасував два останн коміти, але залишив на диску збереженні в них зміни.
HEAD~N -- адресує N-ний коміт назад від поточної голови. Тобто HEAD~1 (або скорочено для одного
коміту HEAD~) адресує попередній коміт, HEAD~3 адресує третій коміт назад від поточного.

### 5.Видалення файлів.
Скористаємось `git rm`, вона одразу видалить файл з файлової системи і додасть факт видалення файлу до індексу.
І закомітимо зміни.
```
$ git rm text.txt 
rm 'text.txt'
$ git status -uno
On branch releases/1.2
Your branch is ahead of 'origin/releases/1.2' by 2 commits.
  (use "git push" to publish your local commits)

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	deleted:    text.txt

Untracked files not listed (use -u option to show untracked files)
$ git commit -m "file text.txt was deleted"
[releases/1.2 8dacdfd] file text.txt was deleted
 1 file changed, 0 insertions(+), 0 deletions(-)
 delete mode 100644 text.txt

```
 
### 6.Переміщення файлів.
Скористаємось `git mv`, вона одразу перемістить файл з файлової системи і додасть факт видалення 
старого файлу і додавання нового до індексу.
І закомітимо зміни.
```
$ git mv program.cs prog
$ git status -uno
On branch releases/1.2
Your branch is ahead of 'origin/releases/1.2' by 3 commits.
  (use "git push" to publish your local commits)

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	renamed:    program.cs -> prog

Untracked files not listed (use -u option to show untracked files)
$ git commit -m "rename of file program.cs to prog.cs"
[releases/1.2 84e17c1] rename of file program.cs to prog.cs
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename program.cs => prog (100%)
$ 

```
### 7.Гілкування.
# 7.1 Створення 3-ох гілок.
Команда `git branch` при передачі їй аргументу трактує аргумент як ім'я гілки, яку
необхідно створити, і створює її з посиланням на поточний коміт, але активною залишається стара
гілка.
```
$ git branch my_branch_1
$ git branch my_branch_2
$ git branch my_branch_3
$ git branch
  my_branch_1
  my_branch_2
  my_branch_3
* releases/1.2

```
Біля гілки, стан коміту якої наразі відображений в робочому дереві, тобто активної гілки, буде
стояти зірочка.
# 7.2 Переключання між гілками.
Команда `git checkout` відповідає за зміну гілки. Відбувається зміна активної
гілки, над якою тепер будуть виконуватися команди, а також робоче дерево приведеться до вигляду
того стану, на який вказує гілка, на яку ми переключилися.
```
$ git checkout my_branch_3
Switched to branch 'my_branch_3'
$ git branch
  my_branch_1
  my_branch_2
* my_branch_3
  releases/1.2

```
Біля гілки, стан коміту якої наразі відображений в робочому дереві, тобто активної гілки, буде
стояти зірочка. Так як зірочка змінила своє положення на новостворену 2-у гілку переключення між 
гілками відбулось.
### 8.Знаходження в історії комітів тих набір комітів, в яких була зміна по конкретному шаблону в конкретному файлі
Оберемо файл treestate.py, в якому є рядок `yield [FILE, path, os.path.getmtime(path)]`, по якому треба довідатися історію його
зміни. Для цього використовується комбінація `git log` та `git diff`.

Спочатку за допомогою `git log` виведемо лише ті коміти, в яких змінювався текст заданий
шаблоном через опцію -G та обмежимо вивід лише файлом, що нас цікавить - treestate.py:

```
$ git log -G 'yield [FILE, path, os.path.getmtime(path)]' treestate.py
commit 8770779bc8b62b7be80fe2d863876bab57c19a13 (grafted, origin/releases/1.2)
Author: theGreatWhiteShark <princess.trudildis@posteo.de>
Date:   Sat Dec 9 18:27:42 2023 +0100

    Merge pull request #1904 from theGreatWhiteShark/phil-driver-resilience
    
    Improve resilience in audio driver handling

```
Тепер переглянемо зміни коміту за допомогою `git show`:
```
$ git show 8770779bc8b62b7be80fe2d863876bab57c19a13 
commit 8770779bc8b62b7be80fe2d863876bab57c19a13 (grafted, origin/releases/1.2)
Author: theGreatWhiteShark <princess.trudildis@posteo.de>
Date:   Sat Dec 9 18:27:42 2023 +0100

    Merge pull request #1904 from theGreatWhiteShark/phil-driver-resilience
    
    Improve resilience in audio driver handling

diff --git a/.appveyor.yml b/.appveyor.yml
new file mode 100644
index 0000000..72b61a3
--- /dev/null
+++ b/.appveyor.yml
@@ -0,0 +1,667 @@
+# Appveyor configuration
+#
+# Cache usage
+# -----------
+#
+# The Appveyor build cache is used for:
+#
+#   - ccache on Linux and Windows
+#   - Howebrew with build prerequisites on macOS
+#   - State of /usr with installed prerequisites on Linux
+#
+# The build cache limit is 1Gb which must be shared amongst all platforms,
+# so we must take care about sizes, so we allocate:
+#
